#pragma once

#include <cmath>

const double glob_eps = 0.0001;

const double func_1_1_A = 1.0;
const double func_1_1_B = 1.2;

double func_1_1(double x)
{
    return std::sqrt(x * (3 - x)) / (x + 1);
}
