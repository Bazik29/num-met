#include <cmath>
#include <iostream>

#include "function.hpp"
#include "methods.hpp"
#include "runge.hpp"

int main(int argc, char const* argv[])
{
    const double eps = glob_eps;

    const double a = func_1_1_A;
    const double b = func_1_1_B;
    std::function<double(double)> f_1 = func_1_1;

    const int n = 10;
    const int runge_p = 4;

    std::string name = "Simpson f(x)";
    std::function<double(std::function<double(double)> f, double a, double b, int n)> m_1 = simpson_1;
    runge_calc(name, m_1, runge_p, eps, f_1, a, b, n);

    return 0;
}
