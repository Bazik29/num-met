#pragma once
#include <cmath>
#include <functional>
#include <omp.h>

#include <string>
#include <iostream>
#include <iomanip>

void print(std::string name, int threads, int n, double eps, double runge, double res)
{
    std::cout << "Method: " << name << std::endl;
    std::cout << "Threads: " << threads << std::endl;
    std::cout << "Eps: " << eps << std::endl;
    std::cout << "Runge: " << runge << std::endl;
    std::cout << "N: " << n << std::endl;
    std::cout << "Result: " << std::setprecision(15) << res << std::endl;
}

void runge_calc(
    std::string name,
    std::function<double(std::function<double(double)> f, double a, double b, int n)> method,
    int p, double eps,
    std::function<double(double)> f,
    double a, double b, int n)
{
    int threads = omp_get_num_procs();
    double sum_1 = 0, sum_2 = 0;
    sum_1 = method(f, a, b, n);
    n *= 2;
    sum_2 = method(f, a, b, n);
    double runge = std::fabs(sum_2 - sum_1) / (std::pow(2, p) - 1);

    while (runge >= eps) {
        sum_1 = sum_2;
        n *= 2;
        sum_2 = method(f, a, b, n);
        runge = std::fabs(sum_2 - sum_1) / (std::pow(2, p) - 1);
    }
    print(name, threads, n, eps, runge, sum_2);
}
