#pragma once
#include <functional>
#include <omp.h>

double rectangleL_1(std::function<double(double)> f, double a, double b, int n)
{
    const double h = (b - a) / n;
    double sum = 0.0;
#pragma omp parallel for reduction(+ : sum)
    for (int i = 0; i < n-1; i++) {
        sum += f(a + i * h);
    }
    return (sum * h);
}

double rectangleR_1(std::function<double(double)> f, double a, double b, int n)
{
    const double h = (b - a) / n;
    double sum = 0.0;
#pragma omp parallel for reduction(+ : sum)
    for (int i = 1; i < n; i++) {
        sum += f(a + i * h);
    }
    return (sum * h);
}

double rectangleM_1(std::function<double(double)> f, double a, double b, int n)
{
    const double h = (b - a) / n;
    double sum = 0.0;
#pragma omp parallel for reduction(+ : sum)
    for (int i = 0; i < n; i++) {
        sum += f(a + (i + 0.5) * h);
    }
    return (sum * h);
}

double n38_1(std::function<double(double)> f, double a, double b, int m)
{
    const double h = (b - a) / (3 * m);
    double sum = f(a) + f(b);
    double sum_2 = 0.0;
#pragma omp parallel for reduction(+ : sum_2)
    for (int i = 2; i <= m; i++) {
        sum_2 += f(a + (3 * i - 3) * h);
    }
    double sum_3 = 0.0;
#pragma omp parallel for reduction(+ : sum_3)
    for (int i = 1; i <= m; i++) {
        sum_3 += f(a + (3 * i - 2) * h) + f(a + (3 * i - 1) * h);
    }
    return ((sum + 2 * sum_2 + 3 * sum_3) * h * 3.0 / 8.0);
}

double trapez_1(std::function<double(double)> f, double a, double b, int n)
{
    const double h = (b - a) / (n);
    double sum = 0.0;

#pragma omp parallel for reduction(+ : sum)
    for (int i = 0; i < n; i++) {
        sum += f(a + i * h) + f(a + (i + 1) * h);
    }

    return (0.5 * sum * h);
}

double simpson_1(std::function<double(double)> f, double a, double b, int n){
    const double h = (b - a) / (n);

    double sum = 0.0;
#pragma omp parallel for reduction(+ : sum)
    for(int i = 0; i < n; i++) {
        const double x1 = a + i * h;
        const double x2 = a + (i + 1) * h;

        sum += f(x1) + 4.0 * f((x1 + x2) / 2.0) + f(x2);
    }

    return (sum * h / 6.0); 
}
