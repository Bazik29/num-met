
all: lab_simpson lab_trapez lab_38 lab_reactangle

run: all
	./lab_reactangle
	./lab_trapez
	./lab_38
	./lab_simpson

tofile: all
	./lab_reactangle > log.txt
	echo ----------- >> log.txt
	./lab_trapez >> log.txt
	echo ----------- >> log.txt
	./lab_38 >> log.txt
	echo ----------- >> log.txt
	./lab_simpson >> log.txt

lab_simpson: lab_simpson.cpp function.hpp methods.hpp
	g++ lab_simpson.cpp -o $@ -fopenmp 

lab_trapez: lab_trapez.cpp function.hpp methods.hpp
	g++ lab_trapez.cpp -o $@ -fopenmp 

lab_38: lab_38.cpp function.hpp methods.hpp
	g++ lab_38.cpp -o $@ -fopenmp 

lab_reactangle: lab_reactangle.cpp function.hpp methods.hpp
	g++ lab_reactangle.cpp -o $@ -fopenmp 

clean:
	rm -rf lab_simpson lab_trapez lab_38 lab_reactangle log.txt *.o